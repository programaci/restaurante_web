<?php

use Illuminate\Database\Seeder;

class PlatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $platort = [
            ['10','ALBONDIGAS TERNERA', 'Plato Principal', 'img/logos/prueba.png', '1', '400'],
            ['20','ARROZ BLANCO CON CHAMPINONES', 'Plato Principal', 'img/logos/prueba.png', '1', '110' ],
            ['30','ARROZ PRIMAVERA' , 'Plato Principal', 'img/logos/prueba.png', '1', '320'],
            ['40','BOCADILLO DE CHORIZO', 'Entrada', 'img/logos/prueba.png', '1', '150' ],
            ['50','BOQUERON FRITO', 'Entrada', 'img/logos/prueba.png', '1', '210' ],
            ['60','CARNE CON TOMATE TERNERA', 'Plato Principal', 'img/logos/prueba.png', '1', '420' ],
            ['70','PASTA CON ATUN', 'Plato Principal', 'img/logos/prueba.png', '1', '340' ],
            ['80','ENSALADA DE VERDURAS', 'Entrada', 'img/logos/prueba.png', '1', '250' ],
            ['90','HAMBURGUESA DE POLLO', 'Plato Principal', 'img/logos/prueba.png', '1', '220' ],
            ['100','ENSALADA TROPICAL', 'Entrada', 'img/logos/prueba.png', '1', '195' ],
            ['110','PATATAS CON ALCACHOFAS', 'Plato Principal', 'img/logos/prueba.png', '1', '350' ],
            ['120','POSTRE LACTEO', 'Postre', 'img/logos/prueba.png', '1', '110' ]
            
        ];
        foreach ($platort  as $platortyk)
        {
            $newplato= [
                'codigo' => $platortyk[0],
                'nombre' => $platortyk[1],
                'descripcion' => $platortyk[2],
                'url_logo' => $platortyk[3],
                'disponible' => $platortyk[4],
                'costo' => $platortyk[5]
            ];
            \App\Ability::create($newplato);
        }
    }
}
