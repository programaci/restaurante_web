<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlmacenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rt_almacen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_almacen')->unique();
            $table->string('nombre')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('url_logo')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rt_almacen');
    }
}
