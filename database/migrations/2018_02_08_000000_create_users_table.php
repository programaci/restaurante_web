<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rt_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_users')->unique();
            $table->string('email')->unique();
            $table->string('password')->unique();
            $table->string('telefono')->nullable();
            $table->string('url_logo')->nullable();
            $table->rememberToken();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rt_users');
    }
}
