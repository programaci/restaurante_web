<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rt_transacciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plato_inicial')->unique();
            $table->string('plato_final')->nullable();
            $table->string('descripcion')->nullable();
            $table->unsignedInteger('id_users');
            $table->foreign('id_users')->references('id')->on('rt_users');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rt_transacciones');
    }
}
