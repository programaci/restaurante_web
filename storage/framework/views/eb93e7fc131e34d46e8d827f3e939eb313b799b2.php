<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="">

  <title>Restaurante-Web</title>

  <!-- Bootstrap Core CSS -->
  <link href="<?php echo e(asset('css/bootstrap.css')); ?>" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="<?php echo e(asset('css/generic.css')); ?>" rel="stylesheet">
</head>

<body>
  <div id="app" class="container sidebart">
    <div class="row">
        <!-- Init Header -->
        <!-- Init section web -->
<div id="web" class="col-lg-3 col-md-3 col-sm-12">
</div>
<div id="web" class="col-lg-9 col-md-9 col-sm-12">
  <div class="row">
      
  </div>
</div>
<!-- End section web -->
<!-- Init section mobile -->
<div id="mobile" class="col-sm-12 center-block">
   
</div>
<div id="mobile" class="row center-block">
  <div class="col-sm-12 wine-color-bg font-white-bold">
    <div>
      
    </div>
  </div>
</div>
<!-- End section mobile -->
        <!-- End Header -->
        <!-- Init Web -->
        <div id="web" class="col-lg-3 col-md-3 col-sm-12 sidebart">
          <figure class="detektei">
            <div class="container-detektei">
              
            </div>
          </figure>
          <div class="alinear-center">
           Menú Ejecutivo
          </div>
          <div class="row">
               <div class="content-sidebar">
                   <div class="sidebar">
                       <img src="<?php echo e(asset('img/logos/menu.jpg')); ?>" alt="" width="100%" height="250">
                   </div>
                  <div class="sidebar">
                       <img src="<?php echo e(asset('img/logos/fondo.jpg')); ?>" alt="" width="100%" height="30">
                   </div>
                    <div class="sidebar">
                       <img src="<?php echo e(asset('img/logos/menu2.jpg')); ?>" alt="" width="100%" height="250">
                   </div>
               </div>
          </div>
        </div>
        <div id="web" class="col-lg-9 col-md-9 col-sm-12">
          <!-- Init Content Web -->
          <div class="content-slider1 ">
            <div class="slider1 sidebar">
              <div id="demo" class="carousel slide" data-ride="carousel">
                <!-- The slideshow -->
                                  
                <div class="carousel-inner ">
                  <div class="carousel-item active">
                    <img src="<?php echo e(asset('img/logos/albondigas.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/ensaladatropical.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/pastalcachofas.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/hamburguesa.jpg')); ?>" alt="" width="100%" height="630">
                  </div>                    
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/ensaladaverdura.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/pastatun.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/postre.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/arrozchampi.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/arrozprimavera.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/bocadillochorizo.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/boqueronfrito.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                  <div class="carousel-item">
                    <img src="<?php echo e(asset('img/logos/carneternera.jpg')); ?>" alt="" width="100%" height="630">
                  </div>
                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                  <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                  <span class="carousel-control-next-icon"></span>
                </a>
              </div>
            </div>
          </div>
          <div class="row row-content bg-dark-gray">
           
          </div>
          <div class="row row-content bg-light-gray">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="row" style="height:100%;">
               
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="row">
                               
              </div>
            </div>
          </div>
          <!-- End Content Web -->
        </div>
    </div>
    <!-- End Web -->
    <!-- Init Mobile -->
    <div id="mobile" class="col-sm-12">
      <figure class="detektei">
      <a href="#demo"><div class="container-detektei">
          
        </div></a>
      </figure>
      <div class="alinear-derecha">

      </div>
      <br/>
    </div>
    <div id="mobile">
      <!-- Init Content Mobile -->
     
      <div class="row" style="padding: 10px;">
        
      </div>
      <div class="row bg-dark-gray" style="padding: 10px;">
          <div class="tittle-mobile">
              
          </div>
      </div>
      <div class="row bg-dark-gray">
        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6">
             
            </div>
          </div>
        </div>
      </div>
      <div class="row bg-dark-gray" style="margin-bottom:7px;">

      </div>
      <!-- End Content Mobile -->
    </div>
    <!-- End Mobile -->
    <!-- Init Footer -->
    <div class="row wine-color-bg">
<!-- Init section mobile -->
<div id="mobile" class="center-block">
    <div class="col-sm-12 icons-footer">
        
    </div>
</div>
<!-- End section mobile -->
    </div>
    <!-- End Footer -->
  </div>
</body>
</html>