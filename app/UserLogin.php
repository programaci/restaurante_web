<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLogin extends Model
{
     /**
     * The descriptive table of the process.
     *
     * @var array
     */
    protected $table = 'rt_users_login';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_users'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

}

