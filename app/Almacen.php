<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{
     /**
     * The descriptive table of the process.
     *
     * @var array
     */
    protected $table = 'rt_almacen';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'code_almacen', 'nombre', 'descripcion', 'url_logo'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

}

