<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaccion extends Model
{
     /**
     * The descriptive table of the process.
     *
     * @var array
     */
    protected $table = 'rt_transacciones';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_users', 'plato_inicial', 'plato_final', 'descripcion'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

}

