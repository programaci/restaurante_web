<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plato extends Model
{
     /**
     * The descriptive table of the process.
     *
     * @var array
     */
    protected $table = 'rt_platos';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'codigo', 'nombre', 'descripcion', 'url_logo', 'disponible', 'costo'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    
     /**
     * 
        Ratio of Dish with Ingredients
     *
     * @var array
     */
    public function ingredientes()
    {	
            return $this->hasMany('App\Ingrediente');
    }
    /**
     * 
        Ratio of Dish with Alergeno
     *
     * @var array
     */
    public function alergenos()
    {	
            return $this->hasMany('App\Alergeno');
    }
}

