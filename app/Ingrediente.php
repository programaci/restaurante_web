<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
     /**
     * The descriptive table of the process.
     *
     * @var array
     */
    protected $table = 'rt_ingredientes';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'codigo', 'nombre', 'descripcion', 'tipo', 'url_logo', 'almacen', 'disponible', 'code_plato'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    
    /**
     * Relation of Ingredient with Dish
     *
     * @var array
     */
    public function plato()
    {
            return $this->belongsTo('App\Plato');
    }
}

