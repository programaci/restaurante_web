<?php

namespace App\Http\Controllers\Api;

use App\Plato;
use App\Alergeno;
use App\Ingrediente;
use App\User;
use App\Transaccion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class ApiAllergenController extends Controller
{
    /**
    * Devuelve todos los Alergenos Registrados
    *
    * @return data
    */
    public function index()
    {          
        $alerg = Alergeno::all()->where('disponible',1);
        return response()->json(['status' => 'ok', 'data' => $alerg], 200);
    }
    /**
    * Devuelve todos los datos del Alergeno Solicitado a traves de un ID
    * @param  $id
    * @return data
    */
    public function detail($id)
    {

        $alerg = Alergeno::find($id);
        if (!$alerg) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra Alergeno para este id.'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $alerg], 200);
    }
    /**
    * Devuelve todos los Alergenos asociados a un Plato, solicitado a traves del Codigo del Plato
    * @param  $id
    * @return data
    */
       public function  detailalergenos($id)
    {

        $alergc_plato = Alergeno::select('nombre')->where('code_plato',$id)->get();
        if (count($alergc_plato) == 0 ) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra Alergenos para este Plato'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $alergc_plato], 200);
    }
    /**
    * Devuelve todos los NOMBRES de cada Plato donde esta asociado este Alergeno a traves de un ID
    * @param  $id
    * @return data
    */
    public function  platoalergenos($id)
    {
        $platosrr[]='';
        $platosresult[]='';
        $alerg = Alergeno::find($id); 
        if (count($alerg) == 0 ) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra Platos para este Alergeno'])], 404);
        }

        $alerg_ingred = Alergeno::select('code_plato')->where('codigo',$alerg->codigo)->distinct()->get();
        
        unset($platosrr[0]);   
        foreach ($alerg_ingred as $p) {   
            array_push($platosrr, $p->code_plato);
        }
        
        $alerg_platos = Plato::select('nombre')->whereIn('codigo',$platosrr)->get();
        
        if (count($alerg_platos) == 0 ) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra Platos para este Alergeno'])], 404);
        }

        unset($platosresult[0]); 
        foreach ($alerg_platos as $prs) {   
            array_push($platosresult, $prs->nombre);
        }
       
        return response()->json(['status' => 'ok', 'data' => $platosresult], 200);
        
    }
    /**
    * Crea Alergeno Nuevos
    * @param  $id, Request $request
    * @return data
    */
    public function create($id, Request $request)
    {
        $codealergen = Alergeno::all()->max('codigo');
        $tod=substr($codealergen, 5);
        $todresult=$tod+1;
        
        //Selecciono los Alergenos Iniciales
         $alergc_init = Alergeno::select('nombre')->where('code_plato',$id)->get();
        
        try {
       
            $alergend = new Alergeno();
            $alergend->nombre = $request->input('nombre');
            $alergend->descripcion = $request->input('descripcion');
            $alergend->tipo = $request->input('tipo');
            $alergend->url_logo = 'img/logos/prueba.png';
            $alergend->almacen = $request->input('almacen');
            $alergend->codigo = 'ALB-0'.$todresult;
            $alergend->code_plato = $id;
            $alergend->save();          
            
            /**
            * Transaccion para registrar  el Nuevo Alergeno
            * @param  $id
            * @return data
            */
            $alergc_find = Alergeno::select('nombre')->where('code_plato',$id)->get();
            $transaccion = new Transaccion();
            $transaccion->id_users =  Auth::id();
            $transaccion->plato_inicial = $alergc_init;
            $transaccion->plato_final = $alergc_find;
            $transaccion->descripcion = 'Alergeno';
            $transaccion->save();
            
            
            
            return response()->json(['code' => 200, 'id' => $alergend->id, 'message' => 'Alergeno Registrado con exito'], 200);
            
        } catch (Exception $e) {
         
            return response()->json(['code' => 404, 'message' => 'Hubo un error'], 200);
        }
        
        

    }
    
    /**
    * Actualiza los Datos del Alergeno
    * @param  $id, Request $request
    * @return data
    */
        public function updatealergen($id, Request $request)
    {
        //Selecciono los Alergenos Iniciales
        $platocod = Alergeno::select('code_plato')->where('id',$id)->first();
        $alergc_init = Alergeno::select('nombre')->where('code_plato',$platocod->code_plato)->get();
        
        Alergeno::where('id', '=', $id)->update(['nombre' => $request->input('nombre'),
            'descripcion' => $request->input('descripcion'),
            'tipo' => $request->input('tipo'),
            'almacen' => $request->input('almacen'),
            'disponible' => $request->input('disponible')
            ]);
        
           /**
            * Transaccion para registrar  la actualizacion del  Alergeno
            * @param  $id
            * @return data
            */
            $alergc_find = Alergeno::select('nombre')->where('code_plato',$platocod->code_plato)->get();
            
            $transaccion = new Transaccion();
            $transaccion->id_users =  Auth::id();
            $transaccion->plato_inicial = $alergc_init;
            $transaccion->plato_final = $alergc_find;
            $transaccion->descripcion = 'Alergeno';
            $transaccion->save();
            
        return response()->json(['data' => ['code' => 200, 'message' => 'Modificacion con exito']], 200);
    }

    
    
    /**
    * Elimina un Alergeno
    * @param  $id
    * @return data
    */
        public function deletealergen($id)
    {
        Alergeno::destroy($id);
        return response()->json(['data' => ['code' => 200, 'message' => 'Eliminacion con exito']], 200);
    }
    

    

}
