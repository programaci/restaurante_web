<?php

namespace App\Http\Controllers\Api;

use App\Plato;
use App\Ingrediente;
use App\Alergeno;
use App\User;
use App\Transaccion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class ApiPlatosController extends Controller
{
    /**
    * Devuelve todos los Platos Registrados
    *
    * @return data
    */
    public function index()
    {
        $platos = Plato::all()->where('disponible',1);
        return response()->json(['status' => 'ok', 'data' => $platos], 200);
    }
    
    
    /**
    * Devuelve todos los datos del PLATO solicitado a traves de un ID
    * @param  $id
    * @return data
    */
    public function detail($id)
    {

        $plato = Plato::find($id);
        if (!$plato) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra Plato para este id.'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $plato], 200);
    }
    
    
    /**
    * Devuelve todos los datos del PLATO solicitado a traves de un CODIGO
    * @param  $id
    * @return data
    */
     public function detailcode($id)
    {

        $plato = Plato::where('codigo',$id)->first();
        if (count($plato) == 0 ) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra Plato para este Codigo.'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $plato], 200);
    }
    
    
    /**
    * Devuelve todos los NOMBRES de cada Ingrediente del PLATO solicitado a traves de un CODIGO
    * @param  $id
    * @return data
    */
    public function  detailingredients($id)
    {

        $ingrd_plato = Ingrediente::select('nombre')->where('code_plato',$id)->get();
        if (count($ingrd_plato) == 0 ) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra Ingredientes para este Plato'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $ingrd_plato], 200);
    }
    
    
    /**
    * Devuelve todos los NOMBRES de cada Alérgenos de los Ingredientes del PLATO solicitado a traves de un CODIGO
    * @param  $id
    * @return data
    */
    public function  detailallergens($id)
    {

        $allerg_plato = Alergeno::select('nombre')->where('code_plato',$id)->get();
      
        if (count($allerg_plato) == 0 ) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra Alergenos para este Plato'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $allerg_plato], 200);
    }
    
    
    /**
    * Crea Platos Nuevos en la Tabla rt_platos
    * @param  Request $request
    * @return data
    */
    public function create(Request $request)
    {
        $codeplato = Plato::all()->max('codigo');

        try {
       
            $plat = new Plato();
            $plat->nombre = $request->input('nombre');
            $plat->descripcion = $request->input('descripcion');
            $plat->url_logo = 'img/logos/prueba.png';
            $plat->costo = $request->input('costo');
            $plat->codigo = ($codeplato+10);
            $plat->save();

            return response()->json(['code' => 200, 'id' => $plat->id, 'message' => 'Plato Registrado con exito'], 200);

        } catch (Exception $e) {
         
            return response()->json(['code' => 404, 'message' => 'Hubo un error'], 200);
        }

    }
    
     /**
    * Actualiza los Datos del Plato
    * @param  $id, Request $request
    * @return data
    */
        public function updateplato($id, Request $request)
    {
        Plato::where('id', '=', $id)->update(['nombre' => $request->input('nombre'),
            'descripcion' => $request->input('descripcion'),
            'costo' => $request->input('costo'),
            'disponible' => $request->input('disponible')
            ]);
        return response()->json(['data' => ['code' => 200, 'message' => 'Modificacion con exito']], 200);
    }
      
    /**
    * Elimina un Ingrediente
    * @param  $id
    * @return data
    */
        public function deleteplato($id)
    {
        Plato::destroy($id);
        return response()->json(['data' => ['code' => 200, 'message' => 'Eliminacion con exito']], 200);
    }


}
