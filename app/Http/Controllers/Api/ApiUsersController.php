<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class ApiUsersController extends Controller
{
    use AuthenticatesUsers;
    /**
    * Devuelve todos los Usuarios Registrados
    *
    * @return data
    */
    public function index()
    {
        $users = User::all();
        return response()->json(['status' => 'ok', 'data' => $users], 200);
    }
    /**
    * Devuelve todos los datos de un Usuario solicitado a traves de un ID
    * @param  $id
    * @return data
    */
    public function detail($id)
    {

        $user = User::find($id);
        if (!$user) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentran datos para este id.'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $user], 200);
    }


    /**
    * Actualiza el PASSWORD de un Usuario enviando como parametro ID, Password Nuevo
    * @param  $userid, Request $request
    * @return data
    */
    public function update($userid, Request $request)
    {
        User::where('id', '=', $userid)->update(['password' => bcrypt($request->input('password'))]);
        return response()->json(['data' => ['code' => 200, 'message' => 'Modificacion con exito']], 200);
    }

    

}
