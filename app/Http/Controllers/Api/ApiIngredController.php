<?php

namespace App\Http\Controllers\Api;

use App\Ingrediente;
use App\User;
use App\Transaccion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class ApiIngredController extends Controller
{
    /**
    * Devuelve todos los Ingredientes Registrados
    *
    * @return data
    */
    public function index()
    { 
        $ingred = Ingrediente::all()->where('disponible',1);
        return response()->json(['status' => 'ok', 'data' => $ingred], 200);
    }
    /**
    * Devuelve todos los datos del Ingrediente Solicitado a traves de un ID
    * @param  $id
    * @return data
    */
    public function detail($id)
    {

        $ingred = Ingrediente::find($id);
        if (!$ingred) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra el Ingrediente para este id.'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $ingred], 200);
    }
  
    /**
    * Devuelve todos los NOMBRES de cada Ingrediente del PLATO solicitado a traves de un CODIGO
    * @param  $id
    * @return data
    */
    public function  detailingredients($id)
    {

        $ingrd_plato = Ingrediente::select('nombre')->where('code_plato',$id)->get();
        if (count($ingrd_plato) == 0 ) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra Ingredientes para este Plato'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $ingrd_plato], 200);
    }
    
    
    /**
    * Crea Ingredientes Nuevos
    * @param  $id, Request $request
    * @return data
    */
    public function create($id, Request $request)
    {
        $codeingred = Ingrediente::all()->max('codigo');
        $tod=substr($codeingred, 4);
        $todresult=$tod+1;
        
        //selecciono los Ingredientes Inicales
         $ingred_init = Ingrediente::select('nombre')->where('code_plato',$id)->get();
        try {
       
            $ingredf = new Ingrediente();
            $ingredf->nombre = $request->input('nombre');
            $ingredf->descripcion = $request->input('descripcion');
            $ingredf->tipo = $request->input('tipo');
            $ingredf->url_logo = 'img/logos/prueba.png';
            $ingredf->almacen = $request->input('almacen');
            $ingredf->codigo = 'AL-0'.$todresult;
            $ingredf->code_plato = $id;
            $ingredf->save();
            
            /**
            * Transaccion para registrar  el Nuevo Ingrediente
            * @param  $id
            * @return data
            */
            $ingred_find = Ingrediente::select('nombre')->where('code_plato',$id)->get();
            $transaccion = new Transaccion();
            $transaccion->id_users =  Auth::id();
            $transaccion->plato_inicial = $ingred_init;
            $transaccion->plato_final = $ingred_find;
            $transaccion->descripcion = 'Ingredientes';
            $transaccion->save();

            return response()->json(['code' => 200, 'id' => $ingredf->id, 'message' => 'Ingrediente Registrado con exito'], 200);

        } catch (Exception $e) {
         
            return response()->json(['code' => 404, 'message' => 'Hubo un error'], 200);
        }

    }
    
    /**
    * Actualiza los Datos del Ingrediente
    * @param  $id, Request $request
    * @return data
    */
        public function updateingred($id, Request $request)
    {
        //selecciono los Ingredientes Iniciales  
         $platocod = Ingrediente::select('code_plato')->where('id',$id)->first();
         $ingred_init = Ingrediente::select('nombre')->where('code_plato',$platocod->code_plato)->get();
        
        Ingrediente::where('id', '=', $id)->update(['nombre' => $request->input('nombre'),
            'descripcion' => $request->input('descripcion'),
            'tipo' => $request->input('tipo'),
            'almacen' => $request->input('almacen'),
            'disponible' => $request->input('disponible')
            ]);
        
        /**
        * Transaccion para registrar la actualizacion del Ingrediente
        * @param  $id
        * @return data
        */
        $ingred_find = Ingrediente::select('nombre')->where('code_plato',$platocod->code_plato)->get();
        
            $transaccion = new Transaccion();
            $transaccion->id_users =  Auth::id();
            $transaccion->plato_inicial = $ingred_init;
            $transaccion->plato_final = $ingred_find;
            $transaccion->descripcion = 'Ingredientes';
            $transaccion->save();
        
        return response()->json(['data' => ['code' => 200, 'message' => 'Modificacion con exito']], 200);
    }

    
    
    /**
    * Elimina un Ingrediente
    * @param  $id
    * @return data
    */
        public function deleteingred($id)
    {
        Ingrediente::destroy($id);
        return response()->json(['data' => ['code' => 200, 'message' => 'Eliminacion con exito']], 200);
    }
    


}
