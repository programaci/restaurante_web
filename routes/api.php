<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware(['cors', 'api'])->group(function () {
    Route::get('users/', 'Api\ApiUsersController@index');
    Route::get('user/{id}', 'Api\ApiUsersController@detail');
    Route::post('user/update/{userid}', 'Api\ApiUsersController@update');
    
    Route::get('Platos/', 'Api\ApiPlatosController@index');
    Route::get('Plato/{id}', 'Api\ApiPlatosController@detail');
    Route::get('Detailcode/{id}', 'Api\ApiPlatosController@detailcode');
    Route::get('IngPlato/{id}', 'Api\ApiPlatosController@detailingredients');
    Route::get('AllergPlato/{id}', 'Api\ApiPlatosController@detailallergens');
    Route::post('CrearPlato/', 'Api\ApiPlatosController@create');
    Route::post('UpdatePlato/{id}', 'Api\ApiPlatosController@updateplato');
    Route::get('DeletePlato/{id}', 'Api\ApiPlatosController@deleteplato');
    
    
    Route::get('Ingred/', 'Api\ApiIngredController@index');
    Route::get('Ingred/{id}', 'Api\ApiIngredController@detail');
    Route::get('ListIngred/{id}', 'Api\ApiIngredController@detailingredients');
    Route::post('CrearIngred/{id}', 'Api\ApiIngredController@create');
    Route::post('UpdateIngred/{id}', 'Api\ApiIngredController@updateingred');
    Route::get('DeleteIngred/{id}', 'Api\ApiIngredController@deleteingred');
    
        
    Route::get('Alergen/', 'Api\ApiAllergenController@index');
    Route::get('Alerge/{id}', 'Api\ApiAllergenController@detail');
    Route::get('DetailcodeAlerge/{id}', 'Api\ApiAllergenController@detailalergenos');
    Route::get('CantPlatoAlerg/{id}', 'Api\ApiAllergenController@platoalergenos');
    Route::post('CrearAlergen/{id}', 'Api\ApiAllergenController@create');
    Route::post('UpdateAlergen/{id}', 'Api\ApiAllergenController@updatealergen');
    Route::get('DeleteAlergen/{id}', 'Api\ApiAllergenController@deletealergen');
    
    
});